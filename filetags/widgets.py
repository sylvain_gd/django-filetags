from PIL import Image
from django.forms import Widget, ClearableFileInput
from django.utils.encoding import force_unicode
from django.utils.safestring import mark_safe
from django.forms.util import flatatt
from django.utils.html import escape, conditional_escape
from filetags.models import Tags


class CommaTags(Widget):
    #  Return tags separated by commas
    input_type = None  # Subclasses must define this.

    def render(self, name, value, attrs=None):
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            try:
                tags = Tags.objects.filter(id__in=value).values_list('name',
                                                                     flat=True)
            except:
                value = None
            else:
                value = ', '.join(tags)
                final_attrs['value'] = force_unicode(value)
        if value is None:
            value = ''
        return mark_safe(u'<input%s />' % flatatt(final_attrs))


class ThumbnailFileInput(ClearableFileInput):
    #  If this is image then show thumbnail instead link
    def render(self, name, value, attrs=None):
        substitutions = {
            'initial_text': self.initial_text,
            'input_text': self.input_text,
            'clear_template': '',
            'clear_checkbox_label': self.clear_checkbox_label,
        }
        template = u'%(input)s'
        substitutions['input'] = super(ClearableFileInput, self).render(name, value, attrs)

        if value and hasattr(value, "url"):
            template = self.template_with_initial
            if value.instance.mimetype.name.split('/')[0] == 'image':
                substitutions['initial'] = (u'<img src="%s" />' % (escape(value.instance.thumb())))
            else:
                substitutions['initial'] = (u'<a href="%s">%s</a>'
                                            % (escape(value.url),
                                               escape(force_unicode(value.instance.title))))
            if not self.is_required:
                checkbox_name = self.clear_checkbox_name(name)
                checkbox_id = self.clear_checkbox_id(checkbox_name)
                substitutions['clear_checkbox_name'] = conditional_escape(checkbox_name)
                substitutions['clear_checkbox_id'] = conditional_escape(checkbox_id)
                substitutions['clear'] = CheckboxInput().render(checkbox_name, False, attrs={'id': checkbox_id})
                substitutions['clear_template'] = self.template_with_clear % substitutions

        return mark_safe(template % substitutions)
