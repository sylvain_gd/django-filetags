function search(event, page) {
	var query = new Array();
	$.each($('input[name=tags]').val().split(','), function() {
		if($.trim(this).length > 2) { query.push($.trim(this)); }
	});
	query = query.join(",");
	var type = $('select[name=type]').val();
	if(typeof(page) != 'undefined') { page = page.replace('#', ''); } else { page = 1; }
    if(query) {
    	$('table>tbody').empty();
        $.ajax({
        	type: 'POST',
            url: SEARCH,
            data: {'query': query, 'page': page, 'type': type},
            dataType: 'text',
            success: function(jsonData) {
            	$.each(jQuery.parseJSON(jsonData), function(i, file) {
            		if(typeof(file.next) != 'undefined' || typeof(file.prev) != 'undefined' || typeof(file.page) != 'undefined' || typeof(file.total) != 'undefined' || typeof(file.all) != 'undefined') {
            			if(file.next == null) {
           			    	$('ul.pager > li.next').addClass("disabled");
           			    	$('ul.pager > li.next > a').attr("href", "#");
           			    }
           			    else {
           			    	$('ul.pager > li.next').removeClass("disabled");
           			    	$('ul.pager > li.next > a').attr("href", '#' + file.next);
           			    }
                        if(file.prev == null) {
                        	$('ul.pager > li.previous').addClass("disabled");
                        	$('ul.pager > li.previous > a').attr("href", "#");
                        }
                        else {
                        	$('ul.pager > li.previous').removeClass("disabled");
                        	$('ul.pager > li.previous > a').attr("href", '#' + file.prev);
                        }
                        $('ul.pager > li.page').html(gettext('Page') + ' ' + file.page + ' ' + gettext('of') + ' ' + file.total + ' (' + file.all + ' ' + gettext('elements searched') + ')');
            		}
                    else {
                    	if(file.mimetype.split("/")[0] === 'image') {
                    		q = '<img src="'+file.file+'" alt="'+file.title+'" />';
                    	}
            			else {
            				q = file.title
            			}
            			$('<tr><td><a href="file/'+file.id+'/'+QUERY_STRING+'" title="'+file.title+'">'+q+'</a></td><td>'+file.comment+'</td></tr>').appendTo('table>tbody');
            		}
            	});
            }
        });
    }
}

$('input[name=tags]').bindWithDelay("keyup", function(event) { search.call(this, event, "1"); }, 1000);
$('select[name=type]').change(function(event) { search.call(this, event, "1"); });
$('ul.pager>li').on("click", "a", function(event) { if(!$(this).parent().hasClass("disabled")) {search.call(this, event, $(this).attr("href"));} });
$("button#reset").click(function(){$("input[name=tags]").val("");});