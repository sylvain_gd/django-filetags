$("#id_tags").autocomplete(SEARCH_TAG, {
	minChars: 3,
	inputClass: null,
	remoteDataType: 'json',
	maxItemsToShow: 30,
	delay: 500,
	useDelimiter: ','
});