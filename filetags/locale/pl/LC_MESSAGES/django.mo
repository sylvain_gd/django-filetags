��    -      �  =   �      �     �     �     �     �          	  *        :     ?     E     M  	   V     `     g     s     |  	   �     �     �     �     �     �  ,   �          !     '     0  	   D  "   N     q     �     �     �     �     �  	   �     �     �     �     �     �     �     �     �  �  �     �     �  
   �     �     �     �  .   �     �  	   �  	   	  	             )  
   /     :     B     I  "   U     x          �     �  ,   �     �     �     �     �  	   	  %   	     B	  	   S	     ]	  
   a	     l	     y	     �	     �	     �	     �	     �	     �	     �	     �	     �	                   "                      '          ,         
                   !            -             #   *   	       %                                (       +   &               $      )                       Actions Add Add file Add new file Added Alert Are you sure you want to delete this file? Back Clear Comment Comment  Copy link Delete Delete file Download Edit Edit file Enter tags separated by commas. Error File File (required) File doesn't exists. File is too big. The maximum upload size is  File type not supported. Files MimeType MimeType (required) MimeTypes Minimum tag limit is 3 characters. Name (required) Next No Previous Search files Select Show file Size: Tag Tags Tags (required) Title Title (required) Type Yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2012-05-16 22:24+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2)
 Akcje Dodaj Dodaj plik Dodane nowy plik Dodano Uwaga Jesteś pewnien, że chcesz usunąć ten plik? Wstecz Wyczyść Komentarz Komentarz Kopiuj link Usuń Usuń plik Pobierz Edytuj Edytuj plik Wpisz tagi rodzielone przecinkami. Błąd Plik Plik (wymagane) Plik nie istnieje. Plik jest zbyt duży. Maksymalny rozmiar to  Nieobsługiwany typ pliku. Pliki MimeType MimeType (wymagane) MimeTypes Minimalna długość tagu to 3 znaki. Nazwa (required) Następne Nie Poprzednie Szukaj pliki Wybierz Pokaż plik Rozmiar: Tag Tagi Tagi (wymagane) Tytuł Tytuł (wymagane) Typ Tak 