# coding: utf-8

from django.conf import settings

MIME_TYPES = getattr(settings, 'FILETAGS_MIME_TYPES',
                     {
                      'Images': ['image/jpeg', 'image/png', 'image/gif'],
                      'PDF': ['application/pdf', ]
                      })

MAX_FILE_SIZE = getattr(settings, 'FILETAGS_MAX_FILE_SIZE', 1000)

IMAGE_SIZES = getattr(settings, 'FILETAGS_IMAGE_SIZES',
                      ['500x500', '400x400', '300x300', '200x200', '100x100'])

FILES_DIR = getattr(settings, 'FILETAGS_FILES_DIR', 'filetags/')

ITEMS_FOR_PAGE = getattr(settings, 'FILETAGS_ITEMS_FOR_PAGE', 20)

UPSCALE = getattr(settings, "THUMBNAIL_UPSCALE", True)

MEDIA_ROOT = getattr(settings, "FILETAGS_MEDIA_ROOT", settings.MEDIA_ROOT)

MEDIA_URL = getattr(settings, "FILETAGS_MEDIA_URL", settings.MEDIA_URL)

STATIC_ROOT = getattr(settings, "FILETAGS_STATIC_ROOT", settings.STATIC_ROOT)
