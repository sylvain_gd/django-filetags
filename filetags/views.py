from django.shortcuts import redirect, render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils import simplejson
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.contrib.auth.decorators import login_required, permission_required
from filetags.forms import FileForm
from filetags.models import Tags, Files, MimeTypes
from filetags.settings import MIME_TYPES, ITEMS_FOR_PAGE, IMAGE_SIZES


@login_required
@permission_required('filetags.view_files')
def index(request):
    if request.META.get('QUERY_STRING'):
        query_string = '?%s' % request.META.get('QUERY_STRING')
    else:
        query_string = ''
    return render(request, 'filetags/main.html',
                  {'query_string': query_string})


@login_required
@permission_required('filetags.add_files')
def add_file(request):
    if request.META.get('QUERY_STRING'):
        query_string = '?%s' % request.META.get('QUERY_STRING')
    else:
        query_string = ''
    if request.POST or request.FILES:
        form = FileForm(request.POST, request.FILES)
        if form.is_valid():
            type = request.FILES.get('file').content_type
            mimetype = MimeTypes.objects.get_or_create(name=type)[0]
            file = form.save(commit=False)
            file.user = request.user
            file.mimetype = mimetype
            file.save()
            for t in request.POST.get('tags').split(','):
                if t and not t.isspace():
                    tag = Tags.objects.get_or_create(name=t.strip())[0]
                    file.tags.add(tag)
            if not query_string:
                return redirect(file)
            else:
                redirect_url = reverse('filetags-show-file', args=[file.id])
                full_redirect_url = '%s%s' % (redirect_url, query_string)
            return HttpResponseRedirect(full_redirect_url)
    else:
        form = FileForm()
    return render(request, 'filetags/add.html', {'form': form,
                                                 'query_string': query_string})


@login_required
@permission_required('filetags.delete_files')
def delete_file(request, id):
    if request.META.get('QUERY_STRING'):
        query_string = '?%s' % request.META.get('QUERY_STRING')
    else:
        query_string = ''
    try:
        file = Files.objects.get(id=id)
    except Files.DoesNotExist:
        file = None
    if file and request.POST:
        file.delete()
        if not query_string:
            return redirect('filetags-index')
        else:
            redirect_url = reverse('filetags-index')
            full_redirect_url = '%s%s' % (redirect_url, query_string)
            return HttpResponseRedirect(full_redirect_url)
    return render(request, 'filetags/delete.html',
                  {'file': file, 'query_string': query_string})


@login_required
@permission_required('filetags.change_files')
def edit_file(request, id):
    if request.META.get('QUERY_STRING'):
        query_string = '?%s' % request.META.get('QUERY_STRING')
    else:
        query_string = ''
    try:
        file = Files.objects.get(id=id)
    except Files.DoesNotExist:
        file = None
    else:
        if request.POST:
            form = FileForm(request.POST, request.FILES, instance=file)
            if form.is_valid():
                try:
                    type = request.FILES.get('file').content_type
                except:
                    type = None
                else:
                    mimetype = MimeTypes.objects.get_or_create(name=type)[0]
                file_edit = form.save(commit=False)
                file_edit.user = request.user
                if type:
                    file_edit.mimetype = mimetype
                file_edit.save()
                if request.FILES:
                    if file.mimetype.name.split('/')[0] == 'image':
                        file_edit.thumbs_delete()
                tags = [tag.name for tag in file.tags.all()]
                for t in request.POST.get('tags').split(','):
                    if t and not t.isspace():
                        if t.strip() in tags:
                            tags.remove(t.strip())
                        tag = Tags.objects.get_or_create(name=t.strip())[0]
                        file_edit.tags.add(tag)
                for t in tags:
                    try:
                        tag = Tags.objects.get(name=t)
                    except Tags.DoesNotExist:
                        pass
                    else:
                        file_edit.tags.remove(tag)
                        if tag.files_set.count() == 0:
                            tag.delete()
                if Files.objects.filter(mimetype=file.mimetype).count() == 0:
                    try:
                        mimetype = MimeTypes.objects.get(pk=file.mimetype.pk)
                    except:
                        mimetype = None
                    else:
                        mimetype.delete()
                redirect_url = reverse('filetags-show-file', args=[file_edit.id])
                full_redirect_url = '%s%s' % (redirect_url, query_string)
                return HttpResponseRedirect(full_redirect_url)
        else:
            form = FileForm(instance=file)
    return render(request, 'filetags/add.html',
                  {'form': form, 'query_string': query_string, })


@login_required
@permission_required('filetags.add_files')
def search_tag(request):
    query = request.GET.get('q')
    if query:
        try:
            tags_json = Tags.objects.filter(name__icontains=query)[:30]
        except:
            tags_json = None
        else:
            tags_json = [{'id': t.id, 'value': t.name} for t in tags_json]
    return HttpResponse(simplejson.dumps(tags_json),
                        mimetype='application/json')


@login_required
@permission_required('filetags.view_files')
def search_files(request):
    if request.POST.get('query'):
        query = [x.strip() for x in request.POST.get('query').split(',')
                 if len(x.strip()) > 2]
        query = r'(' + '|'.join(query) + ')'
        types = r'(' + '|'.join(MIME_TYPES[request.POST.get('type')]) + ')'
        files = [{'id': f.id, 'title': f.title, 'file': f.thumb(),
                  'comment': f.comment, 'mimetype': f.mimetype.name}
                 for f in Files.objects.filter(tags__name__iregex=query,
                                               mimetype__name__regex=types).distinct()]
        paginator = Paginator(files, ITEMS_FOR_PAGE)
        try:
            page = int(request.POST.get('page', '1'))
        except ValueError:
            page = 1
        try:
            file_list = paginator.page(page)
        except (EmptyPage, InvalidPage):
            file_list = paginator.page(paginator.num_pages)
        if file_list.has_next():
            next = file_list.next_page_number()
        else:
            next = None
        if file_list.has_previous():
            prev = file_list.previous_page_number()
        else:
            prev = None
        file_list = list(file_list.object_list)
        file_list.append({'next': next, 'prev': prev, 'page': page,
                          'total': paginator.num_pages, 'all': len(files)})
    return HttpResponse(simplejson.dumps(file_list),
                        mimetype='application/json')


@login_required
@permission_required('filetags.view_files')
def show_file(request, id):
    if request.META.get('QUERY_STRING'):
        query_string = '?%s' % request.META.get('QUERY_STRING')
    else:
        query_string = ''
    try:
        file = Files.objects.get(id=id)
    except Files.DoesNotExist:
        file = None
    return render(request, 'filetags/file.html',
                  {'file': file, 'image_sizes': IMAGE_SIZES,
                   'query_string': query_string})
