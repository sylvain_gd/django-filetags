jQuery.validator.addMethod("tags_validate", function(tags, element) {
	too_short_tags = $.grep(tags.split(','), function(n, i) {
		return $.trim(n).length > 0 && $.trim(n).length < 3
	});
	if(too_short_tags.length === 0) { return true; } else { return false; }
}, "Minimum tag limit is 3 characters.");

jQuery.validator.addMethod("file_validate", function(file, element) {
	file = $('input[type=file]').val();
	img = $('img').attr('src');
	if(file === undefined && img === undefined) {
		return false
	}
	else {
		return true
	}
}, "This field is required.");

$("form").validate({
    errorElement: "p",
    errorClass: "error",
    validClass: "success",
    highlight: function(element, errorClass, validClass) {
        $(element).parents().addClass(errorClass).removeClass(validClass);
    },
    unhighlight: function(element, errorClass, validClass) {
        $(element).parents().removeClass(errorClass);
    },
    rules: {
        title: {
            required: true,
            minlength: 7,
            maxlength: 80
        },
        file: {
        	file_validate: true
        },
        tags: {
        	required: true,
        	tags_validate: true
        }
    },
    messages: {
        title: {
            required: gettext('This field is required.'),
            minlength: gettext('Minimum title limit is 7 characters.'),
            maxlength: gettext('Maximum title limit is 80 characters.')
        },
        file: {
        	file_validate: gettext('This field is required.')
        },
        tags: {
        	required: gettext('This field is required.'),
        	tags_validate: gettext('Minimum tag limit is 3 characters.')
        }
    }
});