from django import template
from filetags.settings import MIME_TYPES

register = template.Library()


@register.simple_tag
def types():
    response =  ''.join('<option value="%s">%s</option>' % (k, k)
                        for k in MIME_TYPES.keys())
    return response
