from django.conf.urls import patterns, url
from views import index, add_file, search_tag, search_files, show_file, delete_file, edit_file

js_info_dict = {
    'packages': ('filetags', ),
}

urlpatterns = patterns('',
    url(r'^$', index, name='filetags-index'),
    url(r'^add-file/$', add_file, name='filetags-add-file'),
    url(r'^search-tag/$', search_tag, name='filetags-search-tag'),
    url(r'^search-files/$', search_files, name='filetags-search-files'),
    url('^file/(?P<id>\d+)/$', show_file, name='filetags-show-file'),
    url('^delete-file/(?P<id>\d+)/$', delete_file, name='filetags-delete-file'),
    url('^edit-file/(?P<id>\d+)/$', edit_file, name='filetags-edit-file'),
    (r'^jsi18n/$', 'django.views.i18n.javascript_catalog', js_info_dict),
)
