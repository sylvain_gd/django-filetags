================
django-filetags
================

------------------------------------
Django file manager based on tags.
------------------------------------

Django file manager. Features:

- adding files with own title (which will be the name of file), description and
  tags
- tags with autocompletion
- specify file types that can be uploaded (based on MimeType)
- grouping file types
- edition, files are overwritten with the same name
- search by tags and group of files (jquery, search results change automatically
  when you enter tags and selecting groups of files + pagination)
- ability to generate different sizes of image files
- for each file: preview title, comment, tags, thumbnails, buttons: download, copy 
  url to clipboard (require flash), select (currently works only with CKEditor)
- delete files ( immediately remove thumbnails, unused tags and mimetypes)
- built-in permissions (add, change, delete, view)
- locales: EN, PL

Screenshots:
-------------

- `http://eshlox.net/en/2012/09/04/django-filetags/ <http://eshlox.net/en/2012/09/04/django-filetags/>`_


Dependencies
------------

- Django_ >= 1.4

.. _Django: https://www.djangoproject.com

- sorl-thumbnails_ >= 11.12

.. _sorl-thumbnails: https://github.com/sorl/sorl-thumbnail

- django-widget-tweaks_ >= 1.1.1

.. _django-widget-tweaks: https://bitbucket.org/treyhunner/django-widget-tweaks/

- Unidecode_ >= 0.04.9

.. _Unidecode: http://pypi.python.org/pypi/Unidecode

- django-compressor_ >= 1.2a1 (optional)

.. _django-compressor: https://github.com/jezdez/django_compressor


Installation
------------

From pypi::

    $ pip install django-filetags

or::

    $ easy_install django-filetags

Configuration
-------------

- Add filetags to ``PYTHONPATH`` and installed applications in settings.py::

    INSTALLED_APPS = (
        ...
        'filetags'
    )

- Add filetags to urls.py::

    url(r'^filetags/', include('filetags.urls')),

- Run ``manage.py syncdb`` to create the tables.

- Run ``manage.py collectstatic`` to copy the static files.

- Application is avaiable at ``/filetags/``.

Settings
---------

- Set the supported file formats.

  Default::

      FILETAGS_MIME_TYPES = {'Images': ['image/jpeg', 'image/png', 'image/gif'],
                              'PDF': ['application/pdf', ]}

  You can find all media types in /etc/mime.types (Linux).
  
  The key is the name used in template. Call it what you want.
  
  Value is list of media types.

- Set max file size in bytes.

  Default::
  
      FILETAGS_MAX_FILE_SIZE = 1000

- Set the size of image files that will be used to create thumbnails.

  Default::
  
      FILETAGS_IMAGE_SIZES = ['500x500', '400x400', '300x300', '200x200', '100x100']

  **Important**:
  Set sizes from largest to smallest. The last size will be used in search list so
  it shouldn't be too large.
  
- Set the name of the directory where files are saved.

  Default::

      FILETAGS_FILES_DIR = 'filetags/'

- Set the number of files displayed on a single search page.

  Default::
  
      FILETAGS_ITEMS_FOR_PAGE = 20

CKEditor
---------

If filetags will be run from CKEditor then add an additional button that allows you to 
copy a file url directly to editor.


  Example::
  
        filebrowserBrowseUrl : '/filetags/',
        filebrowserUploadUrl : '/filetags/add-file/',

