"""
Code from Mezzanine CMS.
https://github.com/stephenmcd/mezzanine/blob/master/mezzanine/core/templatetags/mezzanine_tags.py#L40
Date: 2012-07-15
"""

from django import template
from django.conf import settings

register = template.Library()


if "compressor" in settings.INSTALLED_APPS:
    @register.tag
    def compress(parser, token):
        """
        Shadows django-compressor's compress tag so it can be
        loaded from ``compress``, allowing us to provide
        a dummy version when django-compressor isn't installed.
        """
        from compressor.templatetags.compress import compress
        return compress(parser, token)
else:
    @register.tag
    def compress(parsed, context, token):
        """
        Dummy tag for fallback when django-compressor isn't installed.
        """
        return parsed
