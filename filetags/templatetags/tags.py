from django import template


register = template.Library()


@register.filter
def tags(value):
    if value:
        return ', '.join(value.values_list('name', flat=True))
