from django.contrib import admin
from filetags.models import Tags, Files, MimeTypes


class TagsAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    search_fields = ['name', 'slug']
    prepopulated_fields = {'slug': ('name', )}


class FilesAdmin(admin.ModelAdmin):
    list_display = ('title', 'user', 'comment', 'added')
    search_fields = ['title', 'user', 'comment', 'added', 'tags']


class MimeTypesAdmin(admin.ModelAdmin):
    list_display = ('name', )
    search_fields = ['name', ]


admin.site.register(Tags, TagsAdmin)
admin.site.register(Files, FilesAdmin)
admin.site.register(MimeTypes, MimeTypesAdmin)
