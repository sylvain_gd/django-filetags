$(document).ready(function(){
    $('a.copy').zclip({
        path: STATIC_URL + 'filetags/swf/ZeroClipboard.swf',
        copy: function(){ return $(this).attr('href')},
        afterCopy: function(){}
    });
});

function ProtectPath(path) {
    path = path.replace( /\\/g,'\\\\');
    path = path.replace( /'/g,'\\\'');
    return path ;
}

function gup( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.href);
  if(results == null)
    return "";
  else
    return results[1];
}

function OpenFile(fileUrl) {
    var CKEditorFuncNum = gup('CKEditorFuncNum');
    window.top.opener.CKEDITOR.tools.callFunction(CKEditorFuncNum,encodeURI(fileUrl).replace('#','%23'));
    window.top.close();
    window.top.opener.focus();
}

$('img').each(function(index, value) {
    width_div = $('div.well').width();
    width_img = $(this).width();
    if(width_img > width_div) {
        $(this).css({"width": width_div - 10, "height": "auto"});
    }
});