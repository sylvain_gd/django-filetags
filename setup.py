# coding=utf-8
from setuptools import setup, find_packages


setup(
    name = 'django_filetags',
    version = '0.0.1',
    author = 'Przemysław "eshlox" Kołodziejczyk',
    author_email = 'eshlox@gmail.com',
    packages = find_packages(),
    url = 'https://bitbucket.org/eshlox/django-filetags',
    license = 'BSD',
    keywords = "django filetags file manager",
    description = 'Django file manager based on tags.',
    long_description = open('README.rst').read(),
    include_package_data=True,
    classifiers = [
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    zip_safe = False,
    install_requires=[
        #"django-widget-tweaks >= 1.1.1",
        "Unidecode >= 0.04.9",
    ],
)
