from django import template

register = template.Library()


@register.filter
def isimage(value):
        if value.split('/')[0] == 'image':
            return True
        else:
            return False
