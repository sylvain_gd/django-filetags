from django.forms import ModelForm
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext as _
from django.core.files.uploadedfile import InMemoryUploadedFile
from django import forms
from filetags.models import Files
from filetags.settings import MIME_TYPES, MAX_FILE_SIZE
from filetags.widgets import CommaTags, ThumbnailFileInput


class FileForm(ModelForm):
    tags = forms.CharField(label=_('Tags (required)'), required=True,
                           widget=CommaTags)

    class Meta:
        model = Files
        fields = ('title', 'file', 'tags', 'comment')
        widgets = {
            'file': ThumbnailFileInput,
        }

    def clean_file(self):
        file = self.cleaned_data.get('file', False)
        if file:
            if isinstance(file, InMemoryUploadedFile):
                FILE_SIZE_MAX = MAX_FILE_SIZE * 1024 * 1024
                if file._size > FILE_SIZE_MAX:
                    raise ValidationError(_('File is too big. The maximum upload size is ') + FILE_SIZE_MAX + 'M.')
                if not any(file.content_type in k for k in MIME_TYPES.values()):
                    raise ValidationError(_('File type not supported.'))
            return file
        else:
            raise ValidationError(_('Couldn\'t read uploaded file.'))

    def clean_tags(self):
        tags = self.cleaned_data.get('tags', False)
        if tags:
            for t in tags.split(','):
                if t and not t.isspace():
                    if len(t.strip()) < 3:
                        raise ValidationError(_('Minimum tag limit is 3 characters.'))
