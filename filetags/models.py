# coding=utf-8
from unidecode import unidecode
from sorl.thumbnail import get_thumbnail, delete
from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import MinLengthValidator
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User
from django.db.models.signals import post_delete, pre_delete
from django.dispatch import receiver
from filetags.settings import IMAGE_SIZES
from filetags.utils import OverwritingFileSystemStorage, get_file_path

fs = OverwritingFileSystemStorage()


class Tags(models.Model):
    name = models.CharField(_(u'Name (required)'), max_length=50, unique=True,
                            blank=False, null=False,
                            validators=[MinLengthValidator(3)])
    slug = models.CharField('Slug', max_length=50, blank=False, null=False)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')
        ordering = ('name', 'slug')

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        self.slug = slugify(unidecode(self.name))
        super(Tags, self).save(*args, **kwargs)


class MimeTypes(models.Model):
    name = models.CharField(_(u'MimeType (required)'), max_length=200,
                            unique=True, blank=False, null=False)

    class Meta:
        verbose_name = _('MimeType')
        verbose_name_plural = _('MimeTypes')
        ordering = ('name', )


class Files(models.Model):
    file = models.FileField(_('File (required)'), storage=fs,
                            upload_to=get_file_path, max_length=255,
                            blank=False, null=False)
    added = models.DateTimeField(_('Added'), auto_now_add=True, editable=False,
                                 blank=False, null=False)
    edited = models.DateTimeField(_('Edited'), auto_now=True, blank=False,
                                  null=False)
    title = models.CharField(_(u'Title (required)'), max_length=80,
                             blank=False, null=False,
                             validators=[MinLengthValidator(7)])
    comment = models.TextField(_(u'Comment'), blank=True, null=True)
    tags = models.ManyToManyField(Tags, verbose_name=_(u'Tags (required)'),
                                  blank=False, null=False)
    mimetype = models.ForeignKey(MimeTypes, blank=False, null=False)
    user = models.ForeignKey(User, blank=False, null=False)

    def thumb(self):
        """
        Return dependent on mimetype the smallest thumbnail url or file name.
        """
        if self.mimetype.name.split('/')[0] == 'image':
            thumb = get_thumbnail(self.file, IMAGE_SIZES[-1])
            return thumb.url
        else:
            return str(file.name)

    def thumbs_delete(self):
        """
        Delete all thumbnails assigned to this object.
        """
        if self.mimetype.name.split('/')[0] == 'image':
            delete(self.file, delete_file=False)

    class Meta:
        verbose_name = _('File')
        verbose_name_plural = _('Files')
        ordering = ('added', )
        permissions = (
            ("view_files", "Can see available files"),
        )

    @models.permalink
    def get_absolute_url(self):
        return ('filetags-show-file', (), {'id': self.id})


@receiver(pre_delete, sender=Files)
def delete_tags(sender, **kwargs):
    """
    Delete tags assigned to object before delete.!!!!!!!!!!!!!!!!!!!!!1
    """
    f = kwargs.get('instance')
    for tag in f.tags.all():
        if tag.files_set.count() == 1:
            tag.delete()


@receiver(post_delete, sender=Files)
def delete_files(sender, **kwargs):
    f = kwargs.get('instance')
    if Files.objects.filter(mimetype=f.mimetype).count() == 0:
        try:
            mimetype = MimeTypes.objects.get(pk=f.mimetype.pk)
        except MimeTypes.DoesNotExist:
            mimetype = None
        else:
            mimetype.delete()
    if f.file:
        if f.mimetype.name.split('/')[0] == 'image':
            delete(f.file)
        else:
            f.file.delete(save=False)
