from PIL import Image
from django import template
from filetags.settings import UPSCALE


register = template.Library()


@register.filter
def size(value):
    #  Return image size in pixels
    image = Image.open(value)
    size = ('%dx%d') % (image.size[0], image.size[1])
    return size


@register.filter
def check_size(value, file):
    """
    We don't need thumbnails with this same size so remove smaller.
    Don't check if user set sorl-thumbnails UPSCALE = True.
    """
    if not UPSCALE:
        img = Image.open(file)
        size_new = []
        for size in value:
            s = size.split('x')
            if int(img.size[0]) > int(s[0]) and int(img.size[1]) > int(s[1]):
                size_new.append(size)
        return size_new
    else:
        return value
