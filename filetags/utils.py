import os
import itertools
import errno
from datetime import date
from unidecode import unidecode
from django.template.defaultfilters import slugify
from django.core.files.storage import FileSystemStorage
from filetags.settings import FILES_DIR, MEDIA_ROOT


class OverwritingFileSystemStorage(FileSystemStorage):

    def _save(self, name, *args):
        if self.exists(name):
            self.delete(name)
        return super(OverwritingFileSystemStorage, self)._save(name, *args)

    def get_available_name(self, name):
        return name


def get_file_path(instance, filename):
    from filetags.models import Files

    if not instance.id:
        today = date.today()
        path = FILES_DIR + '%s/%s/%s/' % (today.year, today.month, today.day)
        ext = os.path.splitext(filename)[1]
        name = slugify(unidecode(instance.title))
        count = itertools.count(1)
        filename = os.path.join(MEDIA_ROOT, path, "%s%s" % (name, ext))
        while os.path.isfile(filename):
            filename = os.path.join(MEDIA_ROOT, path,
                                    "%s_%s%s" % (name, next(count), ext))
        return filename.replace(MEDIA_ROOT, '')
    else:
        try:
            file = Files.objects.get(id=instance.id)
        except:
            return filename
        else:
            return file.file.name


def mkdir_p(path):
    try:
        os.makedirs(path)
    except OSError as exc:  # Python >2.5
        if exc.errno == errno.EEXIST:
            pass
        else:
            raise
    else:
        os.chmod(path, 0777)
